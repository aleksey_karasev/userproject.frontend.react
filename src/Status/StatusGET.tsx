import { Status } from './Props';
import axios from 'axios';

export const StatusGET = () => {

    const url = 'https://localhost:7224/Enum/statuses';

    const getStatuses = async () => 
    {
        try{

            const res = await axios.get<string[]>(url);

            return res.data.map((status, index) => ({ id: index, name: status }));
        }
        catch {return []; }
    }
   
    return {
        getStatuses
    }
}

export default StatusGET;