import { BaseFilterProps }  from "../Base/BaseProps";

export interface Company {
  id: number;
  name: string;
  createDate: Date;
  lastUpdateDate: Date;
  deleteDate: Date;
}

export interface CompanyFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Company>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

export interface CompanyFilter extends BaseFilterProps {
  name?: string;
}



