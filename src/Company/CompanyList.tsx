import { useState, useEffect } from "react";

import { Company, CompanyFilter } from './Props';
import { CompanyModalWindow } from './CompanyModalWindow';
import CompanyCRUD from './CompanyCRUD'; 
import CompanyValidation from '../Base/BaseValidation'; 
import CompanySnackbar from '../Base/BaseSnackbar'; 
import { BaseFilter } from '../Base/BaseFilter';

import { Collapse, TextField, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

  
export const CompanyList = () => 
{
    //Popup variables are stored here, but messages are set during CRUD operations
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const sortOptions = [
        { value: 'name', label: 'Названию' }
    ];

    const [filter, setFilter] = useState<CompanyFilter>({
        createdAfter: null,
        createdBefore: null,
        sortBy: '',
        isSortAscending: true,
        showDeletedData: false,
        name: ''
    });

    const filterChange = (e: any) => {
        const { name, value, checked } = e.target;
        setFilter(prevState => ({
          ...prevState,
          [name]: name === 'isSortAscending' || name === 'showDeletedData' ? checked : value
        }));
    };
    
    const handleSubmit = (e: any) => {
        e.preventDefault();

        fetchData();
    };

    //download data
    const fetchData = async () => {
        const data = await getAllCompanies(filter);
        setCompanyList(data);
    };

    //CRUD functionality
    const 
    { 
        newCompany, setNewCompany,
        getAllCompanies,
        CreateCompany, 
        EditCompany, 
        DeleteCompany, 
        companyId, setCompanyId,
        handleInputChange

    } = CompanyCRUD(setSnackbarOpen, setSnackbarMessage);

    //Validation
    const 
    { 
        validationSelectedRow

    } = CompanyValidation(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setCompanyList] = useState<Company[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewCompany({});
        setCompanyId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(companyId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(companyId)){ 
            setDeleteWin(true); 
        }
    }

    //Loading a list of companies
    useEffect(() => {

        fetchData();
    }, []);

    //Clicking on a company from the list
    const handleRowClick = (id: number) => {
        setCompanyId(id === companyId ? null : id);

        const selectedCompany = tableData.find(company => company.id === id);
        if (selectedCompany) {
            setNewCompany(selectedCompany);
        }
    };

    const [isVisible, setIsVisible] = useState(true);

    const toggleVisibility = () => {
        setIsVisible(!isVisible);
    };

    const tableCellStyle = { fontSize: '1.2rem' };
    const tableCellStyleData = { fontSize: '1rem' };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Компании</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                        </div>
                    </div>

                    <Button onClick={(toggleVisibility)}>
                        {isVisible ? 'Скрыть поиск' : 'Показать поиск'}
                    </Button>
                    <Collapse in={isVisible} unmountOnExit>
                        <br />
                        <div>
                            <form onSubmit={handleSubmit}>
                                <TextField
                                    label="Название"
                                    type="text"
                                    name="name"
                                    value={filter.name || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />

                            </form>

                            <br />
                            <BaseFilter
                                handleSubmit={handleSubmit}
                                filterChange={filterChange}
                                filter={filter}
                                sortOptions={sortOptions}
                            />
                            <br />
                        </div>
                    </Collapse>

                    <Table aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{fontSize: '1.5rem'}}><b>Название</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата создания</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата изменения</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата удаления</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === companyId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell sx={tableCellStyleData}>{row.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.createDate ? new Date(row.createDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.lastUpdateDate ? new Date(row.lastUpdateDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.deleteDate ? new Date(row.deleteDate).toLocaleDateString() : '-')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <CompanyModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateCompany}
                    newData={newCompany}
                    handleInputChange={handleInputChange}
                    title={"Создание новой компании"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <CompanyModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditCompany}
                    newData={newCompany}
                    handleInputChange={handleInputChange}
                    title={"Редактировать компанию"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <CompanyModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteCompany}
                    newData={newCompany}
                    handleInputChange={handleInputChange}
                    title={"Удаление компании.\n Вы действительно хотите удалить компанию?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <CompanySnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default CompanyList;