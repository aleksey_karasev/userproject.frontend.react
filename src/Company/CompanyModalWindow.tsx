import { CompanyFormModalProps} from './Props';

import React, { useState } from 'react';
import { Button, Box, Modal, TextField, MenuItem, Select, FormControl, InputLabel  } from "@mui/material"


export const CompanyModalWindow: React.FC<CompanyFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled }) => {
    
    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField 
                label="Название"
                name="name"
                value={newData.name || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
        </Box>
    </Modal>
};


export default CompanyModalWindow;