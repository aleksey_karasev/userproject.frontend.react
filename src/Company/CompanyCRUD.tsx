import { useState } from "react";
import { Company } from './Props';
import axios from 'axios';
import { BaseFilterProps }  from "../Base/BaseProps";

export const CompanyCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const [companyId, setCompanyId] = useState<number | null>(null);

    //New company details
    const [newCompany, setNewCompany] = useState<Partial<Company>>({});


    const url = 'https://localhost:7224/Company';

    const getAllCompanies = async (filter?: BaseFilterProps) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: Company[];
                message: string;
            }>({
                method: 'GET',
                url: url,
                params: filter 
            })

            if (res.status == 401) {
                return [];
            }
            else {
                return await res.data.data;
            }
        }
        catch {return []; }
    }
    
    const CreateCompany = async () => {

        await messageHandleCompany('Компания успешно создана', 'Не удалось создать компанию', url, 'POST', newCompany.name, null);
    };

    const EditCompany = async () => {
       
        await messageHandleCompany('Компания успешно изменена', 'Не удалось изменить компанию', url, 'PUT', newCompany.name, companyId);
    };

    const DeleteCompany = async () => {
        
        await messageHandleCompany('Компания успешно удалена', 'Не удалось удалить компанию', url, 'DELETE', '', companyId);
    };

    //After the request, it displays a message about the work done
    const messageHandleCompany = async (messSeccess: string, messError: string, url: string, p_mehtod: string, companyName?: string, id?:number | null) => {

        var response = await queryCompany(url, p_mehtod, companyName, id)

        if (response instanceof Error) {
            let strError = '';

            if(response?.message.includes('401')){
                strError = 'вы не авторизированы'
            }
            if(response?.message.includes('403')){
                strError = 'у вас недостаточно прав'
            }

            setSnackbarMessage(`${messError}: ${strError}`);
        } else {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        }
    
        openSnackbar(true);
    }

    //Executes the request
    const queryCompany = async (url: string, p_mehtod: string, companyName?: string, id?:number | null) => {
        

        try 
        {
            const apiUrl = id !== null ? `${url}/${id}` : url;

            const res = await axios({
                method: p_mehtod,
                url: apiUrl,
                data: {name: companyName}
            })

            return res;

        } catch (error) {
            console.error("An error occurred:", error); 
            return error;
        }

    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = event.target;
        setNewCompany(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {

        setTimeout(() => {
            window.location.reload();
        }, 500); 
    }

    return {
        newCompany, setNewCompany,
        getAllCompanies,
        CreateCompany, 
        EditCompany, 
        DeleteCompany, 
        companyId, setCompanyId,
        handleInputChange
    }
}

export default CompanyCRUD;