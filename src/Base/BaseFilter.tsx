import React, { useState } from 'react';
import { Button, TextField, MenuItem, Select, FormControl, InputLabel, Checkbox, FormControlLabel } from "@mui/material"
import { BaseFilterProps, FilterProps } from "./BaseProps";



export const BaseFilter = ({ filterChange, handleSubmit, filter, sortOptions }: FilterProps) => {
    
    return (
        <form onSubmit={handleSubmit}>
            <TextField
                label="Создан после"
                type="date"
                name="createdAfter"
                value={filter.createdAfter || ''}
                onChange={filterChange}
                InputLabelProps={{ shrink: true }}
                fullWidth
                style={{ maxWidth: '200px', marginLeft: '10px' }}
            />
            <TextField
                label="Создан до"
                type="date"
                name="createdBefore"
                value={filter.createdBefore || ''}
                onChange={filterChange}
                InputLabelProps={{ shrink: true }}
                fullWidth
                style={{ maxWidth: '200px', marginLeft: '10px' }}
            />
            <FormControl fullWidth style={{ maxWidth: '200px', marginLeft: '10px' }}>
                <InputLabel>Сортировать по</InputLabel>
                <Select name="sortBy" value={filter.sortBy} onChange={filterChange}>
                    <MenuItem value="">None</MenuItem>
                    <MenuItem value="createDate">Дате создания</MenuItem>
                    <MenuItem value="lastUpdateDate">Дате обновления</MenuItem>
                    {sortOptions.map(option => (
                        <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControlLabel
                control={<Checkbox name="isSortAscending" checked={filter.isSortAscending} onChange={filterChange} disabled={!filter.sortBy}/>}
                label="От меньшего к большему" style={{ maxWidth: '300px', marginLeft: '10px' }}
            />
            <FormControlLabel
                control={<Checkbox name="showDeletedData" checked={filter.showDeletedData} onChange={filterChange} />}
                label="Показать удаленные" style={{ maxWidth: '200px' }}
            />
            <Button type="submit" variant="contained" color="primary">Поиск</Button>
        </form>
    );
};

export default BaseFilter;