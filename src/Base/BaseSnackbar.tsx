
import { Snackbar } from "@mui/material";
import { BaseSnackbarProps }  from "./BaseProps";
import  { useEffect, useState } from "react";

const BaseSnackbar = ({ isOpen, message, onClose }: BaseSnackbarProps) => {

    const [snackbarOpen, setSnackbarOpen] = useState(isOpen);

    useEffect(() => {
        setSnackbarOpen(isOpen);
    }, [isOpen]);

    useEffect(() => {

        if (isOpen) {
            
            const timer = setTimeout(() => {
                onClose(); 
            }, 2000);

            //Bringing back the timer clearing function to prevent memory leaks
            return () => clearTimeout(timer);
        }

    }, [isOpen, onClose]);
    
    return <Snackbar
        open={snackbarOpen}
        autoHideDuration={2000}
        onClose={onClose}
        message={message}
    />

};

export default BaseSnackbar;