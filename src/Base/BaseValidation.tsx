

export const BaseValidaton = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const validationSelectedRow = (selectedRow: number | null) =>  {

        if(selectedRow != null) {
            return true;
        }
        else {             
            setSnackbarMessage(`Выберите запись`);
            openSnackbar(true);
            return false;
        }
    }

    return {
        validationSelectedRow
    }
}

export default BaseValidaton;