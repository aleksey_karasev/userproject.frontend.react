export interface FilterProps {
  handleSubmit: (e: any) => void;
  filterChange: (e: any) => void;
  filter: BaseFilterProps;
  sortOptions: { value: string, label: string }[];
}

  export interface BaseSnackbarProps {
    isOpen: boolean;
    message: string;
    onClose: () => void;
  }

  export interface BaseFilterProps {
    createdAfter?: Date | null;
    createdBefore?: Date | null;
    sortBy?: string | null;
    isSortAscending?: boolean;
    showDeletedData?: boolean;
  }