import { ProjectFormModalProps} from './Props';

import React, { useState, useEffect } from 'react';
import { Button, Box, Modal, TextField, MenuItem  } from "@mui/material"
import ProjectSnackbar from '../Base/BaseSnackbar'; 

import { Company } from '../Company/Props';
import CompanyCRUD from '../Company/CompanyCRUD'; 

import { User } from '../User/Props';
import UserCRUD from '../User/UserCRUD'; 

export const ProjectModalWindow: React.FC<ProjectFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled }) => {
    
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const 
    { 
        getAllCompanies
    } = CompanyCRUD(setSnackbarOpen, setSnackbarMessage);

    const [companies, setCompanyList] = useState<Company[]>([]);

    const 
    { 
        getAllUsers
    } = UserCRUD(setSnackbarOpen, setSnackbarMessage);

    const [users, setUserList] = useState<User[]>([]);

    //download data
    const fetchData = async () => {
        const dataCompanies = await getAllCompanies();
        setCompanyList(dataCompanies);

        const dataUsers = await getAllUsers();
        setUserList(dataUsers);
    };

    useEffect(() => {

        fetchData();
    }, []);


    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField 
                label="Название"
                name="name"
                value={newData.name || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                select
                label="Компания-исполнитель"
                name="companyExecutorId"
                value={newData.companyExecutorId  || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {companies.map((company) => (
                    <MenuItem key={company.id} value={company.id}>
                        {company.name}
                    </MenuItem>
                ))}
            </TextField>
            <TextField
                select
                label="Компания-заказчик"
                name="companyCustomerId"
                value={newData.companyCustomerId  || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {companies.map((company) => (
                    <MenuItem key={company.id} value={company.id}>
                        {company.name}
                    </MenuItem>
                ))}
            </TextField>
            <TextField
                select
                label="Руководитель проекта"
                name="managerId"
                value={newData.managerId  || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {users.map((user) => (
                    <MenuItem key={user.id} value={user.id}>
                        {`${user.firstName} ${user.lastName}`}
                    </MenuItem>
                ))}
            </TextField>
            <TextField
                type="date"
                label="Дата начала проекта"
                name="startProject"
                value={newData.startProject ? new Date(newData.startProject).toISOString().slice(0, 10) : '-' || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                type="date"
                label="Дата окончания проекта"
                name="deedline"
                value={newData.deedline ? new Date(newData.deedline).toISOString().slice(0, 10) : '-' || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                type="number"
                label="Приоритет"
                name="priority"
                value={newData.priority || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
            <ProjectSnackbar
                isOpen={snackbarOpen}
                message={snackbarMessage}
                onClose={() => setSnackbarOpen(false)}
            />
        </Box>
    </Modal>
};


export default ProjectModalWindow;