import { useState } from "react";
import { Project } from './Props';
import axios from 'axios';
import { BaseFilterProps }  from "../Base/BaseProps";

export const ProjectCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const [projectId, setProjectId] = useState<number | null>(null);

    //New project details
    const [newProject, setNewProject] = useState<Partial<Project>>({});

    const url = 'https://localhost:7224/Project';

    const getAllProjects = async (filter?: BaseFilterProps) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: Project[];
                message: string;
            }>({
                method: 'GET',
                url: url,
                params: filter 
            })

            return res?.data?.data;
        }
        catch {return []; }
    }
    
    const CreateProject = async () => {

        await messageHandleProject('Проект успешно создан', 'Не удалось создать проект', url, 'POST', newProject, null);
    };

    const EditProject = async () => {
       
        await messageHandleProject('Проект успешно изменен', 'Не удалось изменить проект', url, 'PUT', newProject, projectId);
    };

    const DeleteProject = async () => {
        
        await messageHandleProject('Проект успешно удален', 'Не удалось удалить проект', url, 'DELETE', null, projectId);
    };

    //After the request, it displays a message about the work done
    const messageHandleProject = async (messSeccess: string, messError: string, url: string, p_mehtod: string, data?: Partial<Project> | null, id?:number | null) => {

        var response = await queryProject(url, p_mehtod, data, id)

        if (response instanceof Error) {
            let strError = '';

            if(response?.message.includes('401')){
                strError = 'вы не авторизированы'
            }
            if(response?.message.includes('403')){
                strError = 'у вас недостаточно прав'
            }

            setSnackbarMessage(`${messError}: ${strError}`);
        } else {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        }
    
        openSnackbar(true);
    }

    //Executes the request
    const queryProject = async (url: string, p_mehtod: string, data?: Partial<Project> | null, id?:number | null) => {
        
        try 
        {
            const apiUrl = id !== null ? `${url}/${id}` : url;

            const res = await axios({
                method: p_mehtod,
                url: apiUrl,
                data: {
                   name: data?.name,
                   companyExecutorId: data?.companyExecutorId,
                   companyCustomerId: data?.companyCustomerId,
                   managerId: data?.managerId,
                   startProject: data?.startProject,
                   deedline: data?.deedline,
                   priority: data?.priority,
                }
            })

            return res;

        } catch (error) {
            console.error("An error occurred:", error); 
            return error;
        }

    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = event.target;
        
        setNewProject(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {

        setTimeout(() => {
            window.location.reload();
        }, 500); 
    }

    return {
        newProject, setNewProject,
        getAllProjects,
        CreateProject, 
        EditProject, 
        DeleteProject, 
        projectId, setProjectId,
        handleInputChange
    }
}

export default ProjectCRUD;