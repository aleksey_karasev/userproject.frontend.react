import { useState, useEffect } from "react";

import { Project, ProjectFilter } from './Props';
import { ProjectModalWindow } from './ProjectModalWindow';
import ProjectCRUD from './ProjectCRUD'; 
import ProjectValidation from '../Base/BaseValidation'; 
import ProjectSnackbar from '../Base/BaseSnackbar'; 
import { BaseFilter } from '../Base/BaseFilter';

import { Collapse, TextField, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"
import { Link } from 'react-router-dom';

export const ProjectList = () => 
{
    //Popup variables are stored here, but messages are set during CRUD operations
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const sortOptions = [
        { value: 'name', label: 'Названию' },
        { value: 'priority', label: 'Приоритету' }
    ];

    const [filter, setFilter] = useState<ProjectFilter>({
        createdAfter: null,
        createdBefore: null,
        sortBy: '',
        isSortAscending: true,
        showDeletedData: false,
        name: '',
        startProjectAfter: null,
        startProjectBefor: null,
        priority: null
    });

    const filterChange = (e: any) => {
        const { name, value, checked } = e.target;
        setFilter(prevState => ({
          ...prevState,
          [name]: name === 'isSortAscending' || name === 'showDeletedData' ? checked : value
        }));
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();

        fetchData();
    };

    const fetchData = async () => {
        const data = await getAllProjects(filter);
        setProjectList(data);
    };

    //CRUD functionality
    const 
    { 
        newProject, setNewProject,
        getAllProjects,
        CreateProject, 
        EditProject, 
        DeleteProject, 
        projectId, setProjectId,
        handleInputChange

    } = ProjectCRUD(setSnackbarOpen, setSnackbarMessage);

    //Validation
    const 
    { 
        validationSelectedRow

    } = ProjectValidation(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setProjectList] = useState<Project[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewProject({});
        setProjectId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(projectId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(projectId)){ 
            setDeleteWin(true); 
        }
    }

    //Loading a list of projects
    useEffect(() => {

        fetchData();
    }, []);

    //Clicking on a project from the list
    const handleRowClick = (id: number) => {
        setProjectId(id === projectId ? null : id);
        
        const selectedProject = tableData.find(project => project.id === id);
        if (selectedProject) {
            setNewProject(selectedProject);
        }
    };

    const [isVisible, setIsVisible] = useState(true);

    const toggleVisibility = () => {
        setIsVisible(!isVisible);
    };

    const tableCellStyle = { fontSize: '1.2rem' };
    const tableCellStyleData = { fontSize: '1rem' };

    const styleLint: React.CSSProperties = {
        display: 'inline-block',
        padding: '10px 20px',
        backgroundColor: '#1976d2',
        color: 'white',
        textDecoration: 'none',
        borderRadius: '25px',
        textAlign: 'center',
        cursor: 'pointer',
        transition: 'background-color 0.3s'
    }

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Проекты</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                            <br /> <br />
                            {projectId ? (
                                <Link 
                                    to={{ pathname: "/projectUsers" }}
                                    state= {{ p_projectId: projectId, p_projectName: newProject.name }}
                                    style= {styleLint}
                                >Сотрудники проекта</Link>
                            ) : (
                                <span style={styleLint}>
                                    Для просмотра сотрудников выберите проект
                                </span>
                            )}
                        </div>
                    </div>

                    <Button onClick={(toggleVisibility)}>
                        {isVisible ? 'Скрыть поиск' : 'Показать поиск'}
                    </Button>
                    <Collapse in={isVisible} unmountOnExit>
                    <br />
                        <div>
                            <form onSubmit={handleSubmit}>
                                <TextField
                                    label="Название"
                                    type="text"
                                    name="name"
                                    value={filter.name || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Приоритет"
                                    type="number"
                                    name="priority"
                                    value={filter.priority || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                            </form>

                            <br />
                            <BaseFilter
                                handleSubmit={handleSubmit}
                                filterChange={filterChange}
                                filter={filter}
                                sortOptions={sortOptions}
                            />
                            <br />
                        </div>
                    </Collapse>

                    <Table aria-label='simple table' >
                        <TableHead>
                            <TableRow>
                                <TableCell sx={tableCellStyle}><b>Название</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Компания-исполнитель</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Компания-заказчик</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Руководитель проекта</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата начала проекта</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата окончания проекта</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Приоритет</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата создания</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата изменения</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата удаления</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === projectId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell sx={tableCellStyleData}>{row.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.companyExecutor.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.companyCustomer.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{`${row.manager.firstName} ${row.manager.lastName}`}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.startProject ? new Date(row.startProject).toLocaleDateString() : '-'}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.deedline ? new Date(row.deedline).toLocaleDateString() : '-'}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.priority}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.createDate ? new Date(row.createDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.lastUpdateDate ? new Date(row.lastUpdateDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.deleteDate ? new Date(row.deleteDate).toLocaleDateString() : '-')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <ProjectModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateProject}
                    newData={newProject}
                    handleInputChange={handleInputChange}
                    title={"Создание нового проекта"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <ProjectModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditProject}
                    newData={newProject}
                    handleInputChange={handleInputChange}
                    title={"Редактировать проект"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <ProjectModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteProject}
                    newData={newProject}
                    handleInputChange={handleInputChange}
                    title={"Удаление проекта.\n Вы действительно хотите удалить проект?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <ProjectSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default ProjectList;