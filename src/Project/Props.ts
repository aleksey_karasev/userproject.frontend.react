import { BaseFilterProps }  from "../Base/BaseProps";

interface Manager {
  firstName: string;
  lastName: string;
}

interface Company {
  name: string
}

export interface Project {
  id: number;
  name: string;
  companyExecutor: Company;
  companyExecutorId: number;
  companyCustomer: Company;
  companyCustomerId: number;
  manager: Manager;
  managerId: number;
  startProject: Date | null;
  deedline: Date | null;
  priority: number | null;
  createDate: Date;
  lastUpdateDate: Date;
  deleteDate: Date;
}

export interface ProjectFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Project>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

export interface ProjectFilter extends BaseFilterProps {
  name?: string;
  startProjectAfter?: Date| null;
  startProjectBefor?: Date| null;
  priority?: number | null;
}

