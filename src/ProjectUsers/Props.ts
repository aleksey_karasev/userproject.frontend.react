
export interface ProjectUsers {
  id: number;
  projectId: number;
  userId: number;
}

export interface ProjectUsersFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<ProjectUsers>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
  projectName?: string;
}



