import { useState } from "react";
import { ProjectUsers } from './Props';
import { User } from '../User/Props';

import axios from 'axios';

export const ProjectUsersCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const [projectUserId, setProjectUserId] = useState<number | null>(null);

    //New user details
    const [newProjectUser, setNewProjectUser] = useState<Partial<ProjectUsers>>({});

    const urlProjectUsers = 'https://localhost:7224/User/byProject';

    const getAllUsersByProject = async (projectIdQuery: number) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: User[];
                message: string;
            }>({
                method: 'GET',
                url: `${urlProjectUsers}/${projectIdQuery}`
            })

            return res?.data?.data;

        }
        catch {return []; }
    }
    
    const url = 'https://localhost:7224/ProjectUsers';

    const AddUserToProject = async () => {

        await messageHandleProjectUser('Пользователь успешно добавлен в проект', 'Не удалось добавить пользователя в проект', url, 'POST', newProjectUser);
    };

    const RemoveUserFromProject = async () => {
        
        await messageHandleProjectUser('Пользователь успешно удален из проекта', 'Не удалось удалить пользователя из проекта', url, 'DELETE', newProjectUser);
    };

    //After the request, it displays a message about the work done
    const messageHandleProjectUser = async (messSeccess: string, messError: string, url: string, p_mehtod: string, projectUsers: Partial<ProjectUsers> | null) => {

        var response = await queryUser(url, p_mehtod, projectUsers)

        if (response instanceof Error) {
            let strError = '';

            if(response?.message.includes('401')){
                strError = 'вы не авторизированы'
            }
            if(response?.message.includes('403')){
                strError = 'у вас недостаточно прав'
            }

            setSnackbarMessage(`${messError}: ${strError}`);
        } else {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        }
    
        openSnackbar(true);
    }

    //Executes the request
    const queryUser = async (url: string, p_mehtod: string, projectUsers: Partial<ProjectUsers> | null) => {
        
        try 
        {
            let apiUrl = `${url}`;

            if(p_mehtod === 'DELETE') {

                const resProjectUsers = await axios.get(url, {
                    params: {
                        projectId: projectUsers?.projectId,
                        userId: projectUsers?.userId
                    }
                });

                apiUrl = resProjectUsers.data.data !== null ? `${url}/${resProjectUsers.data.data[0].id}` : url;
            }

            const res = await axios({
                method: p_mehtod,
                url: apiUrl,
                data: {
                    projectId: projectUsers?.projectId,
                    userId: projectUsers?.userId
                }
            })

            return res;
        } catch (error) {
            console.error("An error occurred:", error);
            return error; 
        }

    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = event.target;
        setNewProjectUser(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {

        setTimeout(() => {
            window.location.reload();
        }, 500); 
    }

    return {
        newProjectUser, setNewProjectUser,
        AddUserToProject, RemoveUserFromProject,
        getAllUsersByProject,
        projectUserId, setProjectUserId,
        handleInputChange
    }
}

export default ProjectUsersCRUD;