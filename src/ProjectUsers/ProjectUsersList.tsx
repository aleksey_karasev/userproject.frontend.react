import { useState, useEffect } from "react";

import { User } from '../User/Props';
import { ProjectUsersModalWindow } from './ProjectUsersModalWindow';
import ProjectUsersCRUD from './ProjectUsersCRUD'; 
import UserValidation from '../Base/BaseValidation'; 
import UserSnackbar from '../Base/BaseSnackbar'; 

import { useLocation } from 'react-router-dom';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

export const ProjectUsersList = () => 
{
    const location = useLocation();
    const p_projectId = location.state?.p_projectId;
    const p_projectName = location.state?.p_projectName;
    
    //Popup variables are stored here, but messages are set during CRUD operations
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const [tableData, setUserList] = useState<User[]>([]);

    const fetchData = async () => {

        if(p_projectId !== null) {

            const parseId = p_projectId ? parseInt(p_projectId, 10) : 0;

            const data = await getAllUsersByProject(parseId);

            setUserList(data);
        }
    };

    //CRUD functionality
    const 
    { 
        newProjectUser, setNewProjectUser,
        AddUserToProject, RemoveUserFromProject,
        getAllUsersByProject,
        projectUserId, setProjectUserId,
        handleInputChange

    } = ProjectUsersCRUD(setSnackbarOpen, setSnackbarMessage);

    //Validation
    const 
    { 
        validationSelectedRow

    } = UserValidation(setSnackbarOpen, setSnackbarMessage);
    

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickAdd = () =>
    {
        setNewProjectUser({ userId: undefined, projectId: p_projectId});
        setProjectUserId(null);
        setCreateWin(true);  
    }

    const clickRemove = () =>
    {
        if(validationSelectedRow(projectUserId)){ 
            setDeleteWin(true); 
        }
    }

    //Loading a list of users
    useEffect(() => {

        fetchData();
    }, []);

    //Clicking on a user from the list
    const handleRowClick = (id: number) => {
        setProjectUserId(id === projectUserId ? null : id);

        const selectedUser = tableData.find(user => user.id === id);
        if (selectedUser) {
            setNewProjectUser({userId: selectedUser.id, projectId: p_projectId});
        }
    };


    const tableCellStyle = { fontSize: '1.2rem' };
    const tableCellStyleData = { fontSize: '1rem' };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Сотрудники проекта <br />«{p_projectName}»</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickAdd}>Добавить сотрудника в проект</Button>
                            <br /> <br />
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickRemove}>Удалить сотрудника из проекта</Button>
                            <br /> <br />
                        </div>
                    </div>

                    <Table aria-label='simple table' >
                        <TableHead>
                            <TableRow>
                                <TableCell sx={tableCellStyle}><b>Эл. почта</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Имя</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Фамилия</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Отчество</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === projectUserId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell sx={tableCellStyleData}>{row.email}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.firstName}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.lastName}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.surName}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <ProjectUsersModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={AddUserToProject}
                    newData={newProjectUser}
                    handleInputChange={handleInputChange}
                    title={"Добавление сотрудника в проект"}
                    titleButton={"Добавить"}
                    disabled={false}
                    projectName={p_projectName}
                />
                <ProjectUsersModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={RemoveUserFromProject}
                    newData={newProjectUser}
                    handleInputChange={handleInputChange}
                    title={"Удаление сотрудника из проекта.\n Вы действительно хотите удалить сотрудника из проекта?"}
                    titleButton={"Удалить"}
                    disabled={true}
                    projectName={p_projectName}
                />
                <UserSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default ProjectUsersList;