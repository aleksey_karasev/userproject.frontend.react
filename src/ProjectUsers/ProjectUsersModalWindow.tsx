import { ProjectUsersFormModalProps} from './Props';

import React, { useState, useEffect } from 'react';
import { Button, Box, Modal, TextField, MenuItem} from "@mui/material"
import ProjectSnackbar from '../Base/BaseSnackbar'; 

import { User } from '../User/Props';
import UserCRUD from '../User/UserCRUD'; 


export const ProjectUsersModalWindow: React.FC<ProjectUsersFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled, projectName }) => {
    
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const 
    { 
        getAllUsers
    } = UserCRUD(setSnackbarOpen, setSnackbarMessage);

    const [users, setUserList] = useState<User[]>([]);

    //download data
    const fetchData = async () => {

        const dataUsers = await getAllUsers();
        setUserList(dataUsers);
    };

    useEffect(() => {

        fetchData();
    }, []);

    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField
                label="проект"
                name="projectId"
                value={projectName || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={true}>
            </TextField>
            <TextField
                select
                label="сотрудник проекта"
                name="userId"
                value={newData.userId || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {users.map((user) => (
                    <MenuItem key={user.id} value={user.id}>
                        {`${user.firstName} ${user.lastName}`}
                    </MenuItem>
                ))}
            </TextField>
           
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
            <ProjectSnackbar
                isOpen={snackbarOpen}
                message={snackbarMessage}
                onClose={() => setSnackbarOpen(false)}
            />
        </Box>
    </Modal>
};


export default ProjectUsersModalWindow;