import axios from 'axios';

export const SetToken = (token: string | null) =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

export const SaveTokenInStorage = (token: string) => {

    SetToken(token);
    localStorage.setItem('token', token);
}

export const DeleteToken = () => {
    SetToken("");
    localStorage.setItem('token', "");
}