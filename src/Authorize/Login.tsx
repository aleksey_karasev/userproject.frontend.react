import { useState, Dispatch, SetStateAction } from 'react';
import axios from 'axios';
import { Typography, TextField, Button, styled, Alert  } from '@mui/material';
import { Navigate } from 'react-router-dom';

import { SaveTokenInStorage } from './Token';

const StyledContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  height: '75vh',
});

const StyledForm = styled('form')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  width: '300px',
  padding: '20px',
  border: '1px solid #ccc',
  borderRadius: '8px',
  boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)'
});

const StyledLabel = styled('label')({
  marginBottom: '10px',
});

const StyledInput = styled(TextField)({
  marginBottom: '20px',
});

const StyledButton = styled(Button)({
  marginTop: '10px',
});

interface LoginProps {
  setIsLoggedIn: Dispatch<SetStateAction<boolean>>;
}

const Login = ({ setIsLoggedIn }: LoginProps) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState<string>('');
  const [redirectToTask, setRedirectToTask] = useState(false);

  const changeEmail = (email: string) => {
    setEmail(email);
  }

  const SaveEmailInStorage = (email: string) => {

    localStorage.setItem('email', email);
  }

  const handleLogin = async () => {

    SaveEmailInStorage(email);

    try {

      const res = await axios({
        method: 'POST',
        url: 'https://localhost:7169/identity/login',
        params: {  Email: email, Password: password }
      })

      if(res.status === 200) 
      {
        SaveTokenInStorage(res.data.token);

        setIsLoggedIn(true);

        setRedirectToTask(true);
      }
      else if(res.status === 300){
        setError('У вас недостаточно прав');
      }
      else if(res.status === 400){
        setError('Вы не авторизированы');
      }
      else { 
        setError(res.statusText); 
      }
      
    } 
    catch (error: any) {
      const res = error.response.data;
  
      if (res != null) {
        setError("Ошибка входа. Измените логин и/или пароль");
      } else if (axios.isAxiosError(error)) {
        setError(error.message);
      } else {
        setError('Unknown error occurred');
      }
    }
  };

  return (
    <StyledContainer>
    <Typography variant="h2" style={{ marginBottom: '25px' }}>Авторизация</Typography>
    <StyledForm>
      <StyledLabel>Почта:</StyledLabel>
      <StyledInput
        type="text"
        value={email}
        onChange={e => changeEmail(e.target.value)}
        variant="outlined"
        fullWidth
      />
      <StyledLabel>Пароль:</StyledLabel>
      <StyledInput
        type="password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        variant="outlined"
        fullWidth
      />
      <StyledButton variant="contained" color="primary" onClick={handleLogin}>
        Войти
      </StyledButton>
      {error && (
        <Alert severity="error" style={{ marginTop: '10px' }}>
          {error}
        </Alert>
      )}
    </StyledForm>
    {redirectToTask && <Navigate to="/tasks" />}
  </StyledContainer>
  );
};

export default Login;
