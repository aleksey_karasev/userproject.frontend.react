import { useState, Dispatch, SetStateAction } from 'react';
import axios from 'axios';
import { Button, styled, Alert  } from '@mui/material';
import { Navigate } from 'react-router-dom';

import { DeleteToken } from './Token';

const StyledContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  height: '75vh',
});

const StyledForm = styled('form')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  width: '300px',
  padding: '20px',
  border: '1px solid #ccc',
  borderRadius: '8px',
  boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)'
});

const StyledLabel = styled('label')({
  marginBottom: '10px',
});

const StyledButton = styled(Button)({
  marginTop: '10px',
});

interface LogoutProps {
  setIsLoggedIn: Dispatch<SetStateAction<boolean>>;
}

const Logout = ({ setIsLoggedIn }: LogoutProps) => {
  const [error, setError] = useState<string>('');
  const [redirectToLogin, setRedirectToLogin] = useState(false);

  const handleLogout = async () => {
    try {

      DeleteToken();

      setIsLoggedIn(false);

      setError('');

      setRedirectToLogin(true);

    } catch (error: any) {
      if (axios.isAxiosError(error)) {
        setError('Login error: ' + error.message);
      } else {
        setError('Unknown error occurred');
      }
    }
  };

  return (
    <StyledContainer>
    <StyledForm>
      <StyledLabel>Вы действительно хотите выйти из профиля?</StyledLabel>
      <StyledButton variant="contained" color="primary" onClick={handleLogout}>
        Выйти
      </StyledButton>
      {error && (
        <Alert severity="error" style={{ marginTop: '10px' }}>
          {error}
        </Alert>
      )}
    </StyledForm>
    {redirectToLogin && <Navigate to="/login" />}
  </StyledContainer>
  );
};

export default Logout;
