import { useState, useEffect } from 'react';
import axios from 'axios';
import { Typography, TextField, Button, styled, Checkbox, Alert  } from '@mui/material';
import { Navigate, useLocation  } from 'react-router-dom';

const StyledContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  height: '90vh',
});

const StyledForm = styled('form')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  width: '300px',
  padding: '20px',
  border: '1px solid #ccc',
  borderRadius: '8px',
  boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)'
});

const StyledLabel = styled('label')({
  marginBottom: '10px',
});

const StyledInput = styled(TextField)({
  marginBottom: '20px',
});

const StyledButton = styled(Button)({
  marginTop: '10px',
});



const Register = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isManager, setIsManager] = useState(false);
  const [error, setError] = useState<string>('');
  const [redirectToUser, setRedirectToUser] = useState(false);

  const location = useLocation();

  useEffect(() => {
    
    const { state } = location;
    setEmail(state);

  }, []);

  const handleLogin = async () => {
    try {

      const res = await axios({
        method: 'POST',
        url: 'https://localhost:7169/identity/register',
        params: {  Email: email, Password: password, ConfirmPassword: confirmPassword, IsManager: isManager }
      })

      if(res.status == 200) 
      {
        setError('');

        setRedirectToUser(true);
      }
      else if(res.status == 300){
        setError('У вас недостаточно прав');
      }
      else if(res.status == 400){
        setError('Вы не авторизированы');
      }
      else { setError(res.statusText); }
      
    } 
    catch (error: any) {
      const res = error.response.data;
  
      if (res != null) {
        setError("Ошибка регистрации. Измените логин и/или пароль. *Регистрация доступна только администратору.");
      } else if (axios.isAxiosError(error)) {
        setError(error.message);
      } else {
        setError('Unknown error occurred');
      }
    }
  };

  return (
    <StyledContainer>
    <Typography variant="h2" style={{ marginBottom: '25px' }}>Регистрация</Typography>
    <StyledForm>
      <StyledLabel>Почта:</StyledLabel>
      <StyledInput
        type="text"
        value={email}
        onChange={e => setEmail(e.target.value)}
        variant="outlined"
        fullWidth
        disabled={true}
      />
      <StyledLabel>Пароль:</StyledLabel>
      <StyledInput
        type="password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        variant="outlined"
        fullWidth
      />
      <StyledLabel>Повтор пароля:</StyledLabel>
      <StyledInput
        type="password"
        value={confirmPassword}
        onChange={e => setConfirmPassword(e.target.value)}
        variant="outlined"
        fullWidth
      />
      <StyledLabel>Это менеджер:</StyledLabel>
      <Checkbox
        value={isManager}
        onChange={e => setIsManager(e.target.checked)}
      />
      <StyledButton variant="contained" color="primary" onClick={handleLogin}>
        Выполнить
      </StyledButton>
      {error && (
        <Alert severity="error" style={{ marginTop: '10px' }}>
          {error}
        </Alert>
      )}
    </StyledForm>
    {redirectToUser && <Navigate to="/users" />}
  </StyledContainer>
  );
};

export default Register;
