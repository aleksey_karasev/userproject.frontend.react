import { useState, useEffect } from "react";
import  { styled } from '@mui/material';

import { User, UserFilter } from './Props';
import { UserModalWindow } from './UserModalWindow';
import UserCRUD from './UserCRUD'; 
import UserValidation from '../Base/BaseValidation'; 
import UserSnackbar from '../Base/BaseSnackbar'; 
import { BaseFilter } from '../Base/BaseFilter';

import { Link } from 'react-router-dom';

import { Collapse, TextField, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

const StyledButton = styled(Button)(({ theme }) => ({
    marginRight: theme.spacing(1),
    borderRadius: '20px', 
    padding: theme.spacing(1, 2), 
  }));

export const UserList = () => 
{
    //Popup variables are stored here, but messages are set during CRUD operations
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const sortOptions = [
        { value: 'firstName', label: 'Имя' },
        { value: 'lastName', label: 'Фамилия' }
    ];

    const [filter, setFilter] = useState<UserFilter>({
        createdAfter: null,
        createdBefore: null,
        sortBy: '',
        isSortAscending: true,
        showDeletedData: false,
        email: '',
        firstName: '',
        lastName: ''
    });

    const filterChange = (e: any) => {
        const { name, value, checked } = e.target;
        setFilter(prevState => ({
          ...prevState,
          [name]: name === 'isSortAscending' || name === 'showDeletedData' ? checked : value
        }));
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();

        fetchData();
    };

    const fetchData = async () => {
        const data = await getAllUsers(filter);
        setUserList(data);
    };

    //CRUD functionality
    const 
    { 
        newUser, setNewUser,
        getAllUsers,
        CreateUser, 
        EditUser, 
        DeleteUser, 
        userId, setUserId,
        handleInputChange

    } = UserCRUD(setSnackbarOpen, setSnackbarMessage);

    //Validation
    const 
    { 
        validationSelectedRow

    } = UserValidation(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setUserList] = useState<User[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewUser({});
        setUserId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(userId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(userId)){ 
            setDeleteWin(true); 
        }
    }

    //Loading a list of users
    useEffect(() => {

        fetchData();
    }, []);

    //Clicking on a user from the list
    const handleRowClick = (id: number) => {
        setUserId(id === userId ? null : id);

        const selectedUser = tableData.find(user => user.id === id);
        if (selectedUser) {
            setNewUser(selectedUser);
        }
    };

    const [isVisible, setIsVisible] = useState(true);

    const toggleVisibility = () => {
        setIsVisible(!isVisible);
    };

    const tableCellStyle = { fontSize: '1.2rem' };
    const tableCellStyleData = { fontSize: '1rem' };

    const styleLint: React.CSSProperties = {
        display: 'inline-block',
        padding: '10px 20px',
        backgroundColor: '#1976d2',
        color: 'white',
        textDecoration: 'none',
        borderRadius: '25px',
        textAlign: 'center',
        cursor: 'pointer',
        transition: 'background-color 0.3s'
    }

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Пользователи</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                            <br /> <br />
                            {newUser.email ? (
                                <Link 
                                    to={{ pathname: "/register" }}
                                    state= { newUser.email }
                                    style={styleLint}
                                >Зарегистрировать</Link>
                            ) : (
                                <span style={styleLint}>
                                    Для регистрации выберите пользователя
                                </span>
                            )}
                        </div>
                    </div>

                    <Button onClick={(toggleVisibility)}>
                        {isVisible ? 'Скрыть поиск' : 'Показать поиск'}
                    </Button>
                    <Collapse in={isVisible} unmountOnExit>
                    <br />
                        <div>
                            <form onSubmit={handleSubmit}>
                                <TextField
                                    label="Эл. почта"
                                    type="text"
                                    name="email"
                                    value={filter.email || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Имя"
                                    type="text"
                                    name="firstName"
                                    value={filter.firstName || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Фамилия"
                                    type="text"
                                    name="lastName"
                                    value={filter.lastName || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                            </form>

                            <br />
                            <BaseFilter
                                handleSubmit={handleSubmit}
                                filterChange={filterChange}
                                filter={filter}
                                sortOptions={sortOptions}
                            />
                            <br />
                        </div>
                    </Collapse>

                    <Table aria-label='simple table' >
                        <TableHead>
                            <TableRow>
                                <TableCell sx={tableCellStyle}><b>Эл. почта</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Имя</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Фамилия</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Отчество</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата создания</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата изменения</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата удаления</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === userId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell sx={tableCellStyleData}>{row.email}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.firstName}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.lastName}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.surName}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.createDate ? new Date(row.createDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.lastUpdateDate ? new Date(row.lastUpdateDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.deleteDate ? new Date(row.deleteDate).toLocaleDateString() : '-')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <UserModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateUser}
                    newData={newUser}
                    handleInputChange={handleInputChange}
                    title={"Создание нового пользователя"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <UserModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditUser}
                    newData={newUser}
                    handleInputChange={handleInputChange}
                    title={"Редактировать пользователя"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <UserModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteUser}
                    newData={newUser}
                    handleInputChange={handleInputChange}
                    title={"Удаление пользователя.\n Вы действительно хотите удалить пользователя?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <UserSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default UserList;