import { useState } from "react";
import { User, UserFilter } from './Props';
import axios from 'axios';

export const UserCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const [userId, setUserId] = useState<number | null>(null);

    //New user details
    const [newUser, setNewUser] = useState<Partial<User>>({});

    const url = 'https://localhost:7224/User';

    const getAllUsers = async (filter?: UserFilter) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: User[];
                message: string;
            }>({
            method: 'GET',
            url: url,
            params: filter 
            })

            return res?.data?.data;

        }
        catch {return []; }
    }
    
    const urlProjectUsers = 'https://localhost:7224/User/byProject';

    const getAllUsersByProject = async (projectIdQuery: number) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: User[];
                message: string;
            }>({
                method: 'GET',
                url: `${urlProjectUsers}/${projectIdQuery}`
            })

            return res?.data?.data;

        }
        catch {return []; }
    }

    const CreateUser = async () => {

        await messageHandleUser('Пользователь успешно создан', 'Не удалось создать пользователя', url, 'POST', newUser, null);
    };

    const EditUser = async () => {
       
        await messageHandleUser('Пользователь успешно изменен', 'Не удалось изменить пользователя', url, 'PUT', newUser, userId);
    };

    const DeleteUser = async () => {
        
        await messageHandleUser('Пользователь успешно удален', 'Не удалось удалить пользователя', url, 'DELETE', null, userId);
    };

    //After the request, it displays a message about the work done
    const messageHandleUser = async (messSeccess: string, messError: string, url: string, p_mehtod: string, data?: Partial<User> | null, id?:number | null) => {

        var response = await queryUser(url, p_mehtod, data, id)

        if (response instanceof Error) {
            let strError = '';

            if(response?.message.includes('401')){
                strError = 'вы не авторизированы'
            }
            if(response?.message.includes('403')){
                strError = 'у вас недостаточно прав'
            }

            setSnackbarMessage(`${messError}: ${strError}`);
        } else {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        }
    
        openSnackbar(true);
    }

    //Executes the request
    const queryUser = async (url: string, p_mehtod: string, data?: Partial<User> | null, id?:number | null) => {
        
        try 
        {
            const apiUrl = id !== null ? `${url}/${id}` : url;

            const res = await axios({
                method: p_mehtod,
                url: apiUrl,
                data: {
                    email: data?.email,
                    firstName: data?.firstName,
                    lastName: data?.lastName,
                    surName: data?.surName
                }
            })

            return res;
        } catch (error) {
            console.error("An error occurred:", error);
            return error; 
        }

    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = event.target;
        setNewUser(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {

        setTimeout(() => {
            window.location.reload();
        }, 500); 
    }

    return {
        newUser, setNewUser,
        getAllUsers, getAllUsersByProject,
        CreateUser, 
        EditUser, 
        DeleteUser, 
        userId, setUserId,
        handleInputChange
    }
}

export default UserCRUD;