import { BaseFilterProps }  from "../Base/BaseProps";

export interface User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  surName: string;
  createDate: Date;
  lastUpdateDate: Date;
  deleteDate: Date;
}

export interface UserFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<User>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

export interface UserFilter extends BaseFilterProps {
  email?: string;
  firstName?: string;
  lastName?: string;
}


