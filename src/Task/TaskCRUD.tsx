import { useState } from "react";
import { Task } from './Props';
import axios from 'axios';
import { BaseFilterProps }  from "../Base/BaseProps";

export const TaskCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const [taskId, setTaskId] = useState<number | null>(null);

    //New Task details
    const [newTask, setNewTask] = useState<Partial<Task>>({});

    const url = 'https://localhost:7224/TaskProject';

    const getAllTasks = async (filter?: BaseFilterProps) => 
    {
        try{
            const res = await axios<{
                success: boolean;
                data: Task[];
                message: string;
            }>({
            method: 'GET',
            url: url,
            params: filter 
            })

            return res?.data?.data;

        }
        catch {return []; }
    }
    
    const CreateTask = async () => {

        await messageHandleTask('Задача успешно создана', 'Не удалось создать задачу', url, 'POST', newTask, null);
    };

    const EditTask = async () => {
       
        await messageHandleTask('Задача успешно изменена', 'Не удалось изменить задачу', url, 'PUT', newTask, taskId);
    };

    const DeleteTask = async () => {
        
        await messageHandleTask('Задача успешно удалена', 'Не удалось удалить задачу', url, 'DELETE', null, taskId);
    };

    const SetTaskToProject = async () => {

        await messageHandleTask('Задача успешно добавлена/удалена в проект', 'Не удалось добавить задачу в проект', `${url}/SetTaskToProject`, 'PATCH', newTask, taskId);
    };

    const SetExecutorForTask = async () => {

        await messageHandleTask('Для задачи успешно установлен исполнитель', 'Для задачи не удалось установлен исполнителя', `${url}/SetExecutorForTask`, 'PATCH', newTask, taskId);
    };

    const SetStatusTask = async () => {

        await messageHandleTask('Задача успешно установила статус', 'Не удалось установить статус задаче', `${url}/SetStatusTask`, 'PATCH', newTask, taskId);
    };

    //After the request, it displays a message about the work done
    const messageHandleTask = async (messSeccess: string, messError: string, url: string, p_mehtod: string, data?: Partial<Task> | null, id?:number | null) => {

        var response = await QueryTask(url, p_mehtod, data, id)

        if (response instanceof Error) {
            let strError = '';

            if(response?.message.includes('401')){
                strError = 'вы не авторизированы'
            }
            if(response?.message.includes('403')){
                strError = 'у вас недостаточно прав'
            }

            setSnackbarMessage(`${messError}: ${strError}`);
        } else {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        }
    
        openSnackbar(true);
    }

    //Executes the request
    const QueryTask = async (url: string, p_mehtod: string, data?: Partial<Task> | null, id?:number | null) => {
        
        try 
        {
            const apiUrl = id !== null ? `${url}/${id}` : url;

            const res = await axios({
                method: p_mehtod,
                url: apiUrl,
                data: {
                   name: data?.name,
                   userExecutorId: data?.userExecutorId,
                   userAuthorId: data?.userAuthorId,
                   projectId: data?.projectId,
                   status: data?.status,
                   comment: data?.comment,
                   priority: data?.priority,
                }
            })

            return res;

        } catch (error) {

            console.error("An error occurred:", error); 
            return error;
        }
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = event.target;
        setNewTask(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {

        setTimeout(() => {
            window.location.reload();
        }, 500); 
    }

    return {
        newTask, setNewTask,
        getAllTasks,
        CreateTask, 
        EditTask, 
        DeleteTask, 
        SetTaskToProject,
        SetExecutorForTask,
        SetStatusTask,
        taskId, setTaskId,
        handleInputChange
    }
}

export default TaskCRUD;