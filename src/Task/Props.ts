import { BaseFilterProps }  from "../Base/BaseProps";

interface UserName {
  firstName: string;
  lastName: string;
}

interface ProjectName {
  id: number;
  name: string;
}

export interface Task {
  id: number;
  name: string;
  userExecutor: UserName;
  userExecutorId: number;
  userAuthor: UserName;
  userAuthorId: number;
  project: ProjectName;
  projectId: number | null;
  status: string;
  comment: string;
  priority: number;
  createDate: Date;
  lastUpdateDate: Date;
  deleteDate: Date;
}

export interface TaskFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Task>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
  projectIdFromUsers?: number;
}

export interface TaskFilter extends BaseFilterProps {
  name?: string;
  userExecutorLastName?: string;
  projectName?: string;
  status?: string;
  priority?: number | null;
}

