import { TaskFormModalProps} from './Props';

import React, { useState, useEffect } from 'react';
import { Button, Box, Modal, TextField, MenuItem} from "@mui/material"

import ProjectSnackbar from '../Base/BaseSnackbar'; 
import { Project } from '../Project/Props';
import ProjectCRUD from '../Project/ProjectCRUD'; 

export const TaskProjectModalWindow: React.FC<TaskFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled }) => {
    
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const 
    { 
        getAllProjects
    } = ProjectCRUD(setSnackbarOpen, setSnackbarMessage);

    const [projects, setProjectList] = useState<Project[]>([]);

    //download data
    const fetchData = async () => {

        const dataProjects = await getAllProjects();
        setProjectList(dataProjects);
    };

    useEffect(() => {

        fetchData();
    }, []);

    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField
                select
                label="Проект задачи"
                name="projectId"
                value={newData.projectId || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {projects.map((project) => (
                    <MenuItem key={project.id} value={project.id}>
                        {project.name}
                    </MenuItem>
                ))}
            </TextField>
           
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
            <ProjectSnackbar
                isOpen={snackbarOpen}
                message={snackbarMessage}
                onClose={() => setSnackbarOpen(false)}
            />
        </Box>
    </Modal>
};


export default TaskProjectModalWindow;