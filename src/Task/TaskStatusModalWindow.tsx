import { TaskFormModalProps} from './Props';

import React, { useState, useEffect } from 'react';
import { Button, Box, Modal, TextField, MenuItem} from "@mui/material"

import { Status } from '../Status/Props';
import StatusGET from '../Status/StatusGET'; 

export const TaskStatusModalWindow: React.FC<TaskFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled }) => {
    
    const 
    { 
        getStatuses
    } = StatusGET();

    const [statuses, setStatusList] = useState<Status[]>([]);

    const fetchData = async () => {

        
        const dataStatuses = await getStatuses();
        setStatusList(dataStatuses);
    };

    useEffect(() => {

        fetchData();
    }, []);

    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
           <TextField
                select
                label="Статус"
                name="status"
                value={newData.status || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {statuses.map((status) => (
                    <MenuItem key={status.id + 1} value={status.id + 1}>
                        {status.name}
                    </MenuItem>
                ))}
            </TextField>
           
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
        </Box>
    </Modal>
};


export default TaskStatusModalWindow;