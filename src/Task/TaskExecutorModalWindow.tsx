import { TaskFormModalProps} from './Props';

import React, { useState, useEffect } from 'react';
import { Button, Box, Modal, TextField, MenuItem} from "@mui/material"
import ProjectSnackbar from '../Base/BaseSnackbar'; 

import { User } from '../User/Props';
import UserCRUD from '../User/UserCRUD'; 

export const TaskExecutorModalWindow: React.FC<TaskFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled, projectIdFromUsers }) => {
    
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const 
    { 
        getAllUsersByProject
    } = UserCRUD(setSnackbarOpen, setSnackbarMessage);

    const [users, setUserList] = useState<User[]>([]);

    //download data
    const fetchData = async () => {

        const dataUsers = await getAllUsersByProject(projectIdFromUsers ?? 0);
        setUserList(dataUsers);
    };

    useEffect(() => {

        fetchData();
    }, [projectIdFromUsers]);

    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
            >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField
                select
                label="Исполнитель задачи"
                name="userExecutorId"
                value={newData.userExecutorId || ''}    
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}>
                {users.map((user) => (
                    <MenuItem key={user.id} value={user.id}>
                        {`${user.firstName} ${user.lastName}`}
                    </MenuItem>
                ))}
            </TextField>
           
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
            <ProjectSnackbar
                isOpen={snackbarOpen}
                message={snackbarMessage}
                onClose={() => setSnackbarOpen(false)}
            />
        </Box>
    </Modal>
};


export default TaskExecutorModalWindow;