import { useState, useEffect } from "react";
import  { styled } from '@mui/material';

import { Task, TaskFilter } from './Props';
import { TaskModalWindow } from './TaskModalWindow';
import { TaskProjectModalWindow } from './TaskProjectModalWindow';
import { TaskExecutorModalWindow } from './TaskExecutorModalWindow';
import { TaskStatusModalWindow } from './TaskStatusModalWindow';

import TaskCRUD from './TaskCRUD'; 
import TaskValidation from '../Base/BaseValidation'; 
import TaskSnackbar from '../Base/BaseSnackbar'; 
import { BaseFilter } from '../Base/BaseFilter';

import { TextField, Collapse, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

const StyledButton = styled(Button)(({ theme }) => ({
    marginRight: theme.spacing(1),
    borderRadius: '20px', 
    padding: theme.spacing(1, 2), 
  }));

export const TaskList = () => 
{
    //Popup variables are stored here, but messages are set during CRUD operations
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const sortOptions = [
        { value: 'name', label: 'Названию' },
        { value: 'project', label: 'Проекту' },
        { value: 'userexecutor', label: 'Исполнителью' }
    ];

    const [filter, setFilter] = useState<TaskFilter>({
        createdAfter: null,
        createdBefore: null,
        sortBy: '',
        isSortAscending: true,
        showDeletedData: false,
        name: '',
        userExecutorLastName: '',
        projectName: '',
        status: '',
        priority: null
    });

    const filterChange = (e: any) => {
        const { name, value, checked } = e.target;
        setFilter(prevState => ({
          ...prevState,
          [name]: name === 'isSortAscending' || name === 'showDeletedData' ? checked : value
        }));
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();

        fetchData();
    };

    const fetchData = async () => {
        const data = await getAllTasks(filter);
        setTaskList(data);
    };

    //CRUD functionality
    const 
    { 
        newTask, setNewTask,
        getAllTasks,
        CreateTask, 
        EditTask, 
        DeleteTask, 
        SetTaskToProject,
        SetExecutorForTask,
        SetStatusTask,
        taskId, setTaskId,
        handleInputChange

    } = TaskCRUD(setSnackbarOpen, setSnackbarMessage);

    //Validation
    const 
    { 
        validationSelectedRow

    } = TaskValidation(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setTaskList] = useState<Task[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);
    const [showSetTaskToProjectWin, setSetTaskToProjectWin] = useState<boolean>(false);
    const [showRemoveTaskFromProjectWin, setRemoveTaskFromProjectWin] = useState<boolean>(false);
    const [showSetExecutorForTaskWin, setSetExecutorForTaskWin] = useState<boolean>(false);
    const [showSetStatusTaskWin, setSetStatusTasWin] = useState<boolean>(false);

    const [projectIdFromUsers, setProjectIdFromUsers] = useState<number>(0);

    const clickCreate = () =>
    {
        setNewTask({});
        setTaskId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(taskId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(taskId)){ 
            setDeleteWin(true); 
        }
    }

    const clickSetTaskToProject = () =>
    {
        if(validationSelectedRow(taskId)){ 
            setSetTaskToProjectWin(true);    
        }
    }

    const clickRemoveTaskFromProject = () =>
    {
        if(validationSelectedRow(taskId)){ 
            newTask.projectId = null;
            setNewTask(newTask) 
            setRemoveTaskFromProjectWin(true);    
        }
    }

    const clickSetExecutor = () =>
    {
        if(validationSelectedRow(taskId)){ 
            setSetExecutorForTaskWin(true);    
        }
    }

    const clickSetStatus = () =>
    {
        if(validationSelectedRow(taskId)){ 
            setSetStatusTasWin(true);    
        }
    }

    //Loading a list of tasks
    useEffect(() => {

        fetchData();
    }, []);

    //Clicking on a task from the list
    const handleRowClick = (id: number) => {
        setTaskId(id === taskId ? null : id);

        const selectedTask = tableData.find(task => task.id === id);
        if (selectedTask) {
            setNewTask(selectedTask);
            setProjectIdFromUsers(selectedTask?.project?.id ?? 0);
        }
    };

    const [isVisible, setIsVisible] = useState(true);

    const toggleVisibility = () => {
        setIsVisible(!isVisible);
    };

    const tableCellStyle = { fontSize: '1.2rem' };
    const tableCellStyleData = { fontSize: '1rem' };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Задачи</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                            <br /> <br />
                            <StyledButton variant="contained" color="primary" onClick={clickSetTaskToProject}>Добавить задачу в проект</StyledButton>
                            <StyledButton variant="contained" color="primary" onClick={clickRemoveTaskFromProject}>Удалить задачу из проекта</StyledButton>
                            <StyledButton variant="contained" color="info" onClick={clickSetExecutor}>Установить исполнителя</StyledButton>
                            <br /> <br />
                            <StyledButton variant="contained" color="inherit" onClick={clickSetStatus}>Установить статус задачи</StyledButton>
                        
                        </div>

                    </div>

                    <Button onClick={(toggleVisibility)}>
                        {isVisible ? 'Скрыть поиск' : 'Показать поиск'}
                    </Button>
                    <Collapse in={isVisible} unmountOnExit>
                    <br />
                        <div>
                            <form onSubmit={handleSubmit}>
                                <TextField
                                    label="Название"
                                    type="text"
                                    name="name"
                                    value={filter.name || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Фамилия исполнителя"
                                    type="text"
                                    name="userExecutorLastName"
                                    value={filter.userExecutorLastName || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Название проекта"
                                    type="text"
                                    name="projectName"
                                    value={filter.projectName || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Статус"
                                    type="text"
                                    name="status"
                                    value={filter.status || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                                <TextField
                                    label="Приоритет"
                                    type="number"
                                    name="priority"
                                    value={filter.priority || ''}
                                    onChange={filterChange}
                                    InputLabelProps={{ shrink: true }}
                                    fullWidth
                                    style={{ maxWidth: '200px', marginLeft: '10px' }}
                                />
                            </form>

                            <br />
                            <BaseFilter
                                handleSubmit={handleSubmit}
                                filterChange={filterChange}
                                filter={filter}
                                sortOptions={sortOptions}
                            />
                            <br />
                        </div>
                    </Collapse>

                    <Table aria-label='simple table' >
                        <TableHead>
                            <TableRow>
                                <TableCell sx={tableCellStyle}><b>Название</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Исполнитель</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Автор</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Проект</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Статус</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Комментарий</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Приоритет</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата создания</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата изменения</b></TableCell>
                                <TableCell sx={tableCellStyle}><b>Дата удаления</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === taskId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell sx={tableCellStyleData}>{row.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.userExecutor !== null ? `${row.userExecutor?.firstName} ${row.userExecutor?.lastName}` : '')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{`${row.userAuthor?.firstName} ${row.userAuthor?.lastName}`}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.project?.name}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.status}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.comment}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{row.priority}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.createDate ? new Date(row.createDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.lastUpdateDate ? new Date(row.lastUpdateDate).toLocaleDateString() : '-')}</TableCell>
                                    <TableCell sx={tableCellStyleData}>{(row.deleteDate ? new Date(row.deleteDate).toLocaleDateString() : '-')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TaskModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateTask}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Создание новой задачи"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <TaskModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditTask}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Редактировать задачу"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <TaskModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteTask}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Удаление задачи.\n Вы действительно хотите удалить задачу?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <TaskProjectModalWindow
                    isOpen={showSetTaskToProjectWin}
                    buttonClose={() => {setSetTaskToProjectWin(false)}}
                    buttonAction={SetTaskToProject}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Добавить задачу в проект"}
                    titleButton={"Добавить"}
                    disabled={false}
                />
                <TaskProjectModalWindow
                    isOpen={showRemoveTaskFromProjectWin}
                    buttonClose={() => {setRemoveTaskFromProjectWin(false)}}
                    buttonAction={SetTaskToProject}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Удалить задачу из проекта"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <TaskExecutorModalWindow
                    isOpen={showSetExecutorForTaskWin}
                    buttonClose={() => {setSetExecutorForTaskWin(false)}}
                    buttonAction={SetExecutorForTask}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Установить исполнителя задачи. \n  Отображаются только сотрудники проекта"}
                    titleButton={"Установить"}
                    disabled={false}
                    projectIdFromUsers={projectIdFromUsers}
                />
                <TaskStatusModalWindow
                    isOpen={showSetStatusTaskWin}
                    buttonClose={() => {setSetStatusTasWin(false)}}
                    buttonAction={SetStatusTask}
                    newData={newTask}
                    handleInputChange={handleInputChange}
                    title={"Установить статус задачи"}
                    titleButton={"Установить"}
                    disabled={false}
                />
                <TaskSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default TaskList;