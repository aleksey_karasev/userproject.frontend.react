
import { useState, useEffect } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { AppBar, Toolbar, Typography, styled, Box } from '@mui/material';

import Login from './Authorize/Login';
import Logout from './Authorize/Logout';
import Register from './Authorize/Register';
import ProjectList from './Project/ProjectList';
import CompanyList from './Company/CompanyList';
import UserList from './User/UserList';
import TaskList from './Task/TaskList';
import ProjectUsersList from './ProjectUsers/ProjectUsersList';

import { SetToken } from './Authorize/Token';

const StyledAppBar = styled(AppBar)({
  background: '#1976d2',
});

const StyledTypography = styled(Typography)({
  flexGrow: 0,
  fontFamily: 'Arial, sans-serif',
  fontSize: '1.6rem',
  color: '#190A6A',
  fontWeight: '600',
});

const StyledLogin = styled(Typography)({
  flexGrow: 0,
  fontFamily: 'Arial, sans-serif',
  fontSize: '1.2rem',
  color: '#190A6A',
  fontWeight: '600',
  marginLeft: 'auto'
});

const StyledToolbar = styled(Toolbar)({
  display: 'flex',
  justifyContent: 'space-between',
});

const StyledLinkContainer = styled(Box)({
  display: 'flex',
  alignItems: 'center',
});

const StyledAuthorizeContainer = styled(Box)({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
});


const StyledLink = styled(Link)({
  textDecoration: 'none',
  color: '#2C145D',
  margin: '0 1px',
  transition: 'background-color 0.6s ease, box-shadow 0.3s ease', 
  borderRadius: '7px',
  padding: '18px 16px',
  fontSize: '1.2rem', 
  '&:hover': {
    backgroundColor: '#64b5f6', 
    boxShadow: '0 0 4px 0 rgba(0, 0, 0, 0.2)', 
  },
  fontWeight: '500',
});

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [emailView, setEmailView] = useState('');

  useEffect(() => {

    const email = localStorage.getItem('email');

    console.log(email)
    if (email !== null && email !== '') {
      setEmailView(email);
    }

    const token = localStorage.getItem('token');

    if (token !== null && token !== '') {
      SetToken(token);
      setIsLoggedIn(true);
    }

  }, [emailView, isLoggedIn]);

  return (   
    <div className='App'>
      <BrowserRouter>
        <StyledAppBar position="static">
          <StyledToolbar>
            <StyledTypography variant="h6">
              Контроль проектов
            </StyledTypography>
            {isLoggedIn ? (
              <>
              <StyledLinkContainer>
                <StyledLink to="/companies">
                  Компании
                </StyledLink>
                <StyledLink to="/users">
                  Пользователи
                </StyledLink>
                <StyledLink to="/projects">
                  Проекты
                </StyledLink>
                <StyledLink to="/tasks">
                  Задачи
                </StyledLink>
              </StyledLinkContainer>
              <StyledAuthorizeContainer>
                <StyledLogin>
                  {emailView}
                </StyledLogin>
                <StyledLink to="/logout">
                  Выйти
                </StyledLink>
              </StyledAuthorizeContainer>
              </>
            ) : (
              <StyledAuthorizeContainer>
                <StyledLink to="/login">
                  Войти
                </StyledLink>
              </StyledAuthorizeContainer>
            )}
          </StyledToolbar>
        </StyledAppBar>

        <Routes>
          <Route path='/login' element={<Login setIsLoggedIn={setIsLoggedIn} />} />    
          <Route path='/logout' element={<Logout setIsLoggedIn={setIsLoggedIn} />} />    
          <Route path='/register' element={<Register />} />    
          <Route path="/projects" element={<ProjectList />} /> 
          <Route path="/companies" element={<CompanyList />} /> 
          <Route path="/users" element={<UserList />} /> 
          <Route path="/tasks" element={<TaskList />} /> 
          <Route path="/projectUsers" element={<ProjectUsersList />} /> 
        </Routes>
      </BrowserRouter>
    </div>
  ); 
}

export default App;
