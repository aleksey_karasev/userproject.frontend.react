Добрый день!
Это клиентская часть проекта, управления проектами, пользователями проектов, и их задачами.

Описание работы фронта:
Для начала работы необходимо авторизироваться. Изначально существует 3 аккаунта с разными ролями:
1. admin@gmail.com
2. manager@gmail.com
3. employeer@gmail.com

**У всех пароль 123qwe!@#QWE**

После входа открывается навигационное меню, с 4 основными вкладками:
**Компании, Пользователи, Проекты, Задачи**.

* При открытии вкладкии компании появляются кнопки работы с сущностями и
список существующих компаний. Кнопки работают только с ролью администратора,
а отображение компаний работает также при роли менеджера.

* При открытии вкладки пользователи появляются кнопки работы с сущностями, кроме
стандартных CRUD операций появляется кнопка "Зарегистрировать", которая
регистрирует пользователя для входа в систему, и придает ему роль. 
Данную функцию может выполнять только администратор. Этим действием вызывается
сервер авторизации, где добавляется запись о пользователе.

* При открытии вкладки проекты появляются кнопки работы с сущностями и список
существующих проектов. 
	1) Для роля администратор отображаются все проекты в системе.
	2) Для роли менеджер, все проекты где он менеджер или сотрудник.
	3) Для роли сотрудник, только те проекты, где он сотрудник.
Редактировать, создавать или удалять проекты может только адмнистратор.
Так же при нажатии на какой-либо проект появиться кнопка "Сотрудники проекта",
по нажатию на которую, вы передете на страничку работы со связями.
Отобразятся все сотрудники выделенного проекта. Можно как и добавлять новых
сотрудников, так и удалять старых с проекта.

* При открытии вкладки задачи появляются кнопки работы с сущностями и список задач,
кроме стандартных кнопок есть ряд функций для работы с задачами, такие как:
	1) Добавить задачу в проект - требует выбор проекта и нажатию на задачу
из списка задач, доступнка только менеджерам и администраторам.
	2) Удалить задачу из проекта - аналогично с 1 пунктом.
	3) Установить исполнителя - требует выбора пользователя-исполнителя из
текущего проекта. Доступны на выбор только сотрудники проекта. 
Команда доступна администраторам и менеджерам.
	4) Установить статус задачи - меняет статус задачи на один из 3 предложенных,
доступна всем ролям.

Для работы клиента необходимо включить 2 проекта:
UserProject, UserProject.Identity.

Если у вас возникнут какие-либо комментарии по коду, либо рекомендации,
прошу связаться со мной:

по телеграмму: @infurial,
либо в LinkedIn: https://www.linkedin.com/in/aleksei-karasev-912505210/


С уважением
Алексей Карасев

-----------------------------------------

Good afternoon
This is the client part of the project, managing projects, project users, and their tasks.

Front operation description:
To get started, you need to log in. Initially, there are 3 accounts with different roles:
1.admin@gmail.com
2. manager@gmail.com
3. employeer@gmail.com

**Everyone's password is 123qwe!@#QWE**

After logging in, a navigation menu opens with 4 main tabs:
**Companies, Users, Projects, Tasks**.

* When you open the company tab, buttons for working with entities and
list of existing companies. The buttons only work with the administrator role,
and displaying companies also works with the manager role.

* When you open the users tab, buttons for working with entities appear, except
standard CRUD operations, a "Register" button appears, which
registers the user to log in and assigns him a role.
This function can only be performed by an administrator. This action causes
an authorization server where a user record is added.

* When you open the projects tab, buttons for working with entities and a list appear
existing projects.
1) For the administrator role, all projects in the system are displayed.
2) For the manager role, all projects where he is a manager or employee.
3) For the employee role, only those projects where he is an employee.
Only the administrator can edit, create or delete projects.
Also, when you click on any project, the “Project Employees” button will appear,
By clicking on which you will be redirected to the page for working with connections.
All employees of the selected project will be displayed. You can also add new ones
employees and remove old ones from the project.

* When you open the task tab, buttons for working with entities and a list of tasks appear,
In addition to standard buttons, there are a number of functions for working with tasks, such as:
1) Add a task to a project - requires selecting a project and clicking on the task
from the task list, available only to managers and administrators.
2) Remove a task from the project - similarly with point 1.
3) Install executor - requires selecting an executor user from
current project. Only project employees are available to choose from.
The team is available to administrators and managers.
4) Set task status - changes the task status to one of the 3 proposed ones,
Available to all roles.

For the client to work, you need to enable 2 projects:
UserProject, UserProject.Identity.

If you have any comments on the code or recommendations,
please contact me:

by telegram: @infurial,
or on LinkedIn: https://www.linkedin.com/in/aleksei-karasev-912505210/


Sincerely
Alexey Karasev